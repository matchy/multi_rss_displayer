<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	class MultiRssDisplayerBlockController extends RssDisplayerBlockController {

		protected $btTable = 'btMultiRssDisplay';

		/** 
		 * Used for localization. If we want to localize the name/description we have to include this
		 */
		public function getBlockTypeDescription() {
			return t("Fetch, parse and display the contents of Multiple (MAX:10) RSS or Atom feeds.");
		}

		public function getBlockTypeName() {
			return t("Multi RSS Displayer");
		}		

		public function getJavaScriptStrings() {
			return array(
				'feed-address' => t('Please enter a valid feed address.'),
				'feed-num-items' => t('Please enter the number of items to display.')
			);
		}
		
		public function view(){
			if (!is_array($this->url)) {
				$this->url = preg_split("/[\s,]+/", $this->url);
			}
			parent::view();
		}

		public function save($data){
			$buff = preg_split("/[\s,]+/", $data['url']);
			foreach ($buff as $key => $item) {
				if (empty($item)) {
					unset($buff[$key]);
				}
			} 
			$data['url'] = implode("\n", $buff);
			parent::save($data);
		}

		public function getSearchableContent() {
			if (!is_array($this->url)) {
				$this->url = preg_split("/[\s,]+/", $this->url);
			}
			return parent::getSearchableContent();
		}
	}
